package pl.sda.structures;


import pl.sda.structures.arraylist.ListArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // arraylist
        ListArray listArray = new ListArray();
        listArray.add(1);
        listArray.add(2);
        listArray.add(3);
        listArray.add(5);
        listArray.add(10);
        listArray.add(15);
        listArray.add(20);

        listArray.add(1, 100);
        listArray.add(5, 500);
        listArray.add(8, 800);

        System.out.println(listArray);
        listArray.remove(1);
        System.out.println(listArray);

        System.out.println(listArray.contains(5));
        System.out.println(listArray.indexOf(800));

        listArray.set(0, 0);
        System.out.println(listArray);


        // linkedlist
//        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 5));

        // size - zwraca rozmiar listy
        // contains - sprawdza czy element znajduje się w liście
        // indexOf - zwraca indeks elementu podanego w parametrze
        // set - przyjmuje indeks i obiekt i ustawia na pozycji indeks podany obiekt
        // clear - czyści listę

//        System.out.println(list);
    }
}

