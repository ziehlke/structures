package pl.sda.structures.arraylist;

public class ListArray {
    private Object[] array;
    private int size;

    public ListArray() {
        this.array = new Object[5];
        this.size = 0;
    }

    public void add(Object elementToAdd) {
        // 1. s
        if (this.size >= this.array.length) {
            extendArray();
        }
        this.array[this.size] = elementToAdd;

        this.size++;
    }

    /**
     * Usuwa z listy element na podanej pozycji.
     *
     * @param position - indeks elementu do usunięcia.
     */
    public void remove(int position) {

        // w pętli iterujemy od elementu z pozycji usuwanej
        // do ostatniego elementu (size - 1)
        // przestawiamy element z pozycji i+1 na pozycję i

        for (int i = position; i < this.size - 1; i++) {
            this.array[i] = this.array[i + 1];
        }

        this.size--;
    }

    public void add(int position, Object elementToAdd) {
        // sprawdzenie rozmiaru i jeśli zaistnieje potrzeba rozszerzenie tablicy
        if (this.size >= this.array.length) {
            extendArray();
        }

        // przepisuję elementy (od ostatniego do miejsca w które
        // chcę wstawić nowy element
        for (int i = this.size - 1; i >= position; i--) {
            this.array[i + 1] = this.array[i];
        }

        // wstawienie elementu na pozycję 'position'
        this.array[position] = elementToAdd;
        // zwiększenie rozmiaru
        this.size++;
    }

    public Object get(int indeks) {
        return this.array[indeks];
    }

    private void extendArray() {
        // tworzę dwukrotnie większą tablicę
        Object[] arrayCopy = new Object[this.array.length * 2];

        // przepisuję wszystkie elementy z orginalnej tablicy do kopii
        for (int i = 0; i < this.array.length; i++) {
            arrayCopy[i] = this.array[i];
        }

        // zastępuję oryginał kopią
        this.array = arrayCopy;
    }

    @Override
    public String toString() {
        return "ListArray{" +
                "size=" + size +
                "elements=[" + printElements() + "]" +
                '}';
    }

    private String printElements() {
        // pusty string do wypisania
        String elements = "";

        // dla każdego elementu dopisuję go i przecinek
        for (int i = 0; i < this.size; i++) {
            elements += this.array[i] + ", ";
        }

        // zwracam ciąg z elementami
        return elements;
    }

    public void clear() {
        this.size = 0;
    }

    public void removeLast() {
        this.size--;
    }


    // size - zwraca rozmiar listy
    public int size() {
        return this.size;
    }


    // contains - sprawdza czy element znajduje się w liście

    public boolean contains(Object objectToFind) {
        int index = indexOf(objectToFind);

        return index >= 0;

    }

    // indexOf - zwraca indeks elementu podanego w parametrze

    public int indexOf(Object objectToFind) {

        for (int i = 0; i < this.size - 1; i++) {
            if (this.array[i].equals(objectToFind)) {
                return i;
            }
        }
        return -1;
    }

    // set - przyjmuje indeks i obiekt i ustawia na pozycji indeks podany obiekt
    public void set(int indeks, Object objectToInsert) {
        this.array[indeks] = objectToInsert;
    }


}